function calculateMonthlySalary() {
  // input: dailySalary, workingDay
  var dailySalary = document.getElementById("daily-salary").value * 1;
  var workingDay = document.getElementById("working-day").value * 1;

  //progress
  var monthlySalary = dailySalary * workingDay;

  //output: monthlySalary
  document.getElementById(
    "monthly-salary"
  ).innerText = `Lương tháng: ${monthlySalary} VND`;
}
