function calculateSumOfTensAndUnits() {
  //input: a random 2-digit number (twoDigitNumber)
  var twoDigitNumber = document.getElementById("two-digit-number").value * 1;

  //progress
  var tens = Math.floor(twoDigitNumber / 10);
  var units = twoDigitNumber % 10;

  var sumOfTensAndUnits = tens + units;

  //output: sumOfTensAndUnits
  document.getElementById(
    "sum-of-tens-and-units"
  ).innerHTML = `Tổng hàng chục và đơn vị: ${sumOfTensAndUnits}`;
}
