function convertUsdToVnd() {
  //input: currentDollarRate, userDollar
  var currentDollarRate = 23500;
  var userDollar = document.getElementById("user-dollar").value * 1;

  //progress
  var userVnd = currentDollarRate * userDollar;

  //output: userVnd
  document.getElementById("user-vnd").innerText = `${userVnd} VND`;
}
