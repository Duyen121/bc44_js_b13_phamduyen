function calculateArithmeticMean() {
  // input: 5 real numbers
  var number1 = document.getElementById("number-1").value * 1;
  var number2 = document.getElementById("number-2").value * 1;
  var number3 = document.getElementById("number-3").value * 1;
  var number4 = document.getElementById("number-4").value * 1;
  var number5 = document.getElementById("number-5").value * 1;

  // progress
  var arithmeticMean = (number1 + number2 + number3 + number4 + number5) / 5;

  //output: arithmeticMean
  document.getElementById(
    "arithmetic-mean"
  ).innerText = `Trung bình cộng: ${arithmeticMean}`;
}
