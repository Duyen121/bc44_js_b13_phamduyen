function calculateRegtangleCircumferenceAndSuperficies() {
  //input: regtangleWidth, regtangleLength
  var regtangleWidth = document.getElementById("regtangle-length").value * 1;
  var regtangleLength = document.getElementById("regtangle-width").value * 1;

  //progress
  var regtangleCircumference = (regtangleLength + regtangleWidth) * 2;
  var regtangleSuperficies = regtangleLength * regtangleWidth;

  //output: regtangleCircumference, regtangleSuperficies
  document.getElementById(
    "cir-super"
  ).innerHTML = `Chu vi: ${regtangleCircumference} và diện tích: ${regtangleSuperficies}`;
}
